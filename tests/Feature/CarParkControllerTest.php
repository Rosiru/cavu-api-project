<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class CarParkControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate'); // Migrate fresh for each test
    }

     /** @test */
     public function it_can_create_a_booking()
     {
        // Create a user
        $user = factory(User::class)->create();

        // Create a parking number
        $parking = factory(ParkinNumber::class)->create();

        // Set the date range
        $startDate = now();
        $endDate = now()->addDays(5);

        // Simulate a request
        $response = $this->actingAs($user)
            ->json('POST', '/create-booking-route', [
                'date_from' => $startDate->toDateString(),
                'date_to' => $endDate->toDateString(),
                'parking_id' => $parking->id,
            ]);

        // Assert the response
        $response->assertStatus(200)
            ->assertJson([
                'status' => true,
                'message' => 'Successfully Booking!!',
            ]);

        // Assert the booking was created in the database
        $this->assertDatabaseHas('bookings', [
            'user_id' => $user->id,
            'parkin_number_id' => $parking->id,
            'date_from' => $startDate,
            'date_to' => $endDate,
            'status' => 'inprogress',
        ]);
    }
 
}
