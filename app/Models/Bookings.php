<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bookings extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['booking_id', 'user_id', 'parkin_number_id', 'date_from', 'date_to', 'amount', 'status', 'checkin', 'checkout'];
}
