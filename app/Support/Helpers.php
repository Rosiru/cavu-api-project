<?php

namespace App\Support;

use App\Models\ParkingPrice;
use Carbon\CarbonPeriod;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Helpers
{
    public static function PriceCalc($startDate, $endDate)
    {
        $dateRange = CarbonPeriod::create($startDate, $endDate);
        $dateRange = $dateRange->toArray();
        $weekdays=0;
        $weekendDays=0;
        foreach ($dateRange as $date) {
            if ($date->isWeekday()) {
                $weekdays++;
            } else {
                $weekendDays++;
            }
        }
        $parkingPrices = ParkingPrice::all();
        $amount = 0;
        foreach ($parkingPrices as $parkingPrice) {
            if ($parkingPrice->type == 'weekdays') {
                $amount += $weekdays*$parkingPrice->amount ;
            }
            else{
                $amount +=  $weekendDays*$parkingPrice->amount;
            }
        }
        return $amount;
    }

    // public static function isAvailable($id = 0 , string $startDate , string $endDate) 
    // {

    //     return $id;
    //     $status = ['inprogress', 'checkin'];
    //     $data =  DB::table('bookings')->whereRaw('"'.$startDate.'" between `date_from` and `date_to` || "'.$endDate.'" between `date_from` and `date_to`')
    //     ->where('status', $status)->pluck('parkin_number_id');
    //     return $data;
    // }

}
