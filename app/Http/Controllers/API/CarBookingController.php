<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\Bookings;
use Carbon\CarbonPeriod;
use Illuminate\Support\Str;
use App\Models\ParkingPrice;
use App\Models\ParkinNumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookingRequest;
use App\Http\Requests\CheckParkingRequest;
use App\Http\Requests\CheckPricingRequest;
use App\Support\Helpers;

class CarBookingController extends Controller
{

    protected $bookings;

    public function __construct(Bookings $bookings)
    {
        $this->bookings = $bookings;
    }

    public function store(BookingRequest $request)
    {
        $data =[];
        $startDate =  new Carbon($request->date_from);
        $endDate = new Carbon($request->date_to);
        //get user ID
        $data['user_id'] = auth()->user()->id;
        //check parking avaiability
        $parkingId = ParkinNumber::find($request->parking_id);
        $status = ['inprogress', 'checkin'];
        $bookings = Bookings::where(function ($query) use ($startDate, $endDate) { 
            $query->whereBetween('date_from', [$startDate, $endDate])
                ->orWhereBetween('date_to', [$startDate, $endDate])
                ->orWhere(function ($query) use ($startDate, $endDate) {
                    $query->where('date_from', '<', $startDate)
                        ->where('date_to', '>', $endDate);
                });
        })->where('parkin_number_id', $parkingId->id)
        ->where('status', $status)->pluck('parkin_number_id');
        if (!$bookings->isEmpty()) {
            return response()->json([
                'status' => false,
                'message' => 'Sorry!! You cannot booking selected parking No in given daterange',
            ], 401);
        }
        $data['date_from'] = new Carbon($request->date_from);
        $data['date_to'] = $endDate;
        // price calc given date range
        $data['amount'] = Helpers::PriceCalc(Carbon::createFromFormat('Y-m-d', $request->date_from), Carbon::parse($endDate)->format('Y-m-d'));
        $data['booking_id'] = Str::random(6);
        $data['parkin_number_id'] = $parkingId->id;
        $data['status'] = 'inprogress';
        $newBooking = $this->bookings->create($data);
        return response()->json([
            'status' => true,
            'message' => 'Successfully Booking!!',
            'booking_id' => $newBooking->booking_id,
        ], 200);
    }

    public function checkParking(CheckParkingRequest $request) 
    {
        $status = ['inprogress', 'checkin'];
        $startDate =  new Carbon($request->date_from);
        if (!$request->date_to) {
            $endDate = new Carbon($request->date_from);
            $endDate = $endDate->addDays();
        } 
        $endDate = new Carbon($request->date_to);
        try {
            //get parking Queue IDs in given daterange
            $bookings = Bookings::where(function ($query) use ($startDate, $endDate) { $query->whereBetween('date_from', [$startDate, $endDate])
                ->orWhereBetween('date_to', [$startDate, $endDate])
                ->orWhere(function ($query) use ($startDate, $endDate) {
                    $query->where('date_from', '<', $startDate)
                        ->where('date_to', '>', $endDate);
                });
            })->where('status', $status)->pluck('parkin_number_id');
            //get available parking Details
            $availableParkings = ParkinNumber::whereNotIn('id', $bookings)->pluck('name');
            if ($availableParkings) {
                return response()->json([
                    'status' => true,
                    'message' => 'You have available '.$availableParkings->count().' parkings in selected daterange',
                    'data' => $availableParkings
                ], 200);
            }
            return response()->json([
                'status' => true,
                'message' => 'Sorry!! Unfortunately you do not have any parkings in selected daterange',
            ], 406);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function priceDetails(CheckPricingRequest $request) 
    {
        try {
            $prices = ParkingPrice::select('id', 'type', 'amount', 'curruncy')->get();
            // price calc given date range
            $total = Helpers::PriceCalc(Carbon::createFromFormat('Y-m-d', $request->date_from), Carbon::parse($request->date_to)->format('Y-m-d'));
            return response()->json([
                'status' => true,
                'message' => 'Total Amount is '. $total.'£',
                'price details' => $prices
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }

    }

    public function cancelBooking($bookingId) 
    {
        try {
        //Find the Booking
        $booking = Bookings::where('booking_id', $bookingId)->first();

        // Check if the booking exists
        if (!$booking) {
            return response()->json(['error' => 'Booking not found.'], 404);
        }

        // Cancel the Booking
        $booking->update([$booking->status = 'canceled']);
        $booking->delete();

        // Return a Response
        return response()->json(['message' => 'Booking canceled successfully.']);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function amendBooking(BookingRequest $request, $bookingId)
    {
        $startDate =  new Carbon($request->date_from);
        $endDate = new Carbon($request->date_to);
        $parkingId = ParkinNumber::find($request->parking_id);
        $status = ['inprogress', 'checkin'];
        $data = [];
        try {
            $bookings = Bookings::where(function ($query) use ($startDate, $endDate) { $query->whereBetween('date_from', [$startDate, $endDate])
                ->orWhereBetween('date_to', [$startDate, $endDate])
                ->orWhere(function ($query) use ($startDate, $endDate) {
                    $query->where('date_from', '<', $startDate)
                        ->where('date_to', '>', $endDate);
                });
                })->where('parkin_number_id', $parkingId->id)
                ->where('status', $status)->pluck('parkin_number_id');
                if (!$bookings->isEmpty()) {
                    return response()->json([
                        'status' => false,
                        'message' => 'Sorry!! You cannot booking selected parking No in given daterange',
                    ], 401);
                }
            $booking = Bookings::where('booking_id', $bookingId)->where('user_id', auth()->user()->id)->first();

            // Check if the booking exists
            if (!$booking) {
                return response()->json(['error' => 'Booking not found.'], 404);
            }

            $data['date_from'] = new Carbon($request->date_from);
            $data['date_to'] = $endDate;
            // price calc given date range
            $data['amount'] = Helpers::PriceCalc(Carbon::createFromFormat('Y-m-d', $request->date_from), Carbon::parse($endDate)->format('Y-m-d'));
            $data['parkin_number_id'] = $parkingId->id;
            $booking->update($data);
            return response()->json([
                'status' => true,
                'message' => 'Successfully Update Booking!!',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function checkIn($bookingId)
    {
        try {
            //Find the Booking
            $booking = Bookings::where('booking_id', $bookingId)->where('user_id', auth()->user()->id)->whereRaw('"'.Carbon::today().'" between `date_from` and `date_to`')->get();
            if (!$booking->isEmpty()) {
                $data = [];
                $data['status'] = 'checkin';
                $data['checkin'] = Carbon::today();
                $booking->first()->update($data);
                // Return a Response
                return response()->json([ 'status' => true, 'message' => 'Car is being dropped off at the car park'], 200);
            }
            return response()->json([ 'status' => true, 'message' => 'Booking Not Founded'], 401);
    
        } catch (
            \Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function checkout($bookingId)
    {
        try {
            //Find the Booking
            $booking = Bookings::where('booking_id', $bookingId)->where('user_id', auth()->user()->id)->get();
            if (!$booking->isEmpty()) {
                $data = [];
                $data['status'] = 'checkout';
                $data['checkout'] = Carbon::today();
                $booking->first()->update($data);
                // Return a Response
                return response()->json([ 'status' => true, 'message' => 'car picked from the car park'], 200);
            }
            return response()->json([ 'status' => true, 'message' => 'Booking Not Founded'], 401);
    
        } catch (
            \Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

}
