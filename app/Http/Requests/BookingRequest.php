<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date_from' => 'required|date|date_format:Y-m-d|before:date_to',
            'date_to' => 'required|date|date_format:Y-m-d',
            'parking_id' => 'required|numeric|exists:parkin_numbers,id'
        ];
        if ($this->method() !== 'POST') {
            return $rules;
        }
        return $rules;
    }
}
