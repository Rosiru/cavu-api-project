<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckParkingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date_from' => 'required|date|date_format:Y-m-d',
            'date_to' => 'nullable|date|date_format:Y-m-d',
        ];
        return $rules;
    }
}
