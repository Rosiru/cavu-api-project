<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/auth/login', 'API\AuthController@loginUser');

Route::group(['middleware' => ['auth:sanctum']], function(){
    
    Route::group(['prefix' => 'cavu-parking', 'namespace' => 'API'], function () {
        Route::post('/booking', 'CarBookingController@store');
        Route::post('/check-parking', 'CarBookingController@checkParking');
        Route::post('/pricing', 'CarBookingController@priceDetails');
        Route::put('/update/{id}', 'CarBookingController@amendBooking');
        Route::delete('/cancel/{id}', 'CarBookingController@cancelBooking');
        Route::put('/check-in/{id}', 'CarBookingController@checkIn');
        Route::put('/check-out/{id}', 'CarBookingController@checkout');


    });

});
