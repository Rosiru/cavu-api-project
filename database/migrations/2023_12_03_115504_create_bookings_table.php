<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('booking_id')->unique();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('parkin_number_id');
            $table->dateTime('date_from', $precision = 0);
            $table->dateTime('date_to', $precision = 0);
            $table->double('amount', 8, 2);
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();

            // Foreign key constraint
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('parkin_number_id')->references('id')->on('parkin_numbers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
