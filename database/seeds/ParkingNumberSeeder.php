<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParkingNumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parkingQueues = require __DIR__.'/data/parking.php';
        DB::table('parkin_numbers')->insert($parkingQueues);
    }
}
