<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParkingPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parkingPrice = require __DIR__.'/data/price.php';
        DB::table('parking_prices')->insert($parkingPrice);
    }
}
