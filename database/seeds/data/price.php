<?php

use Illuminate\Support\Carbon;

return [
  ['type' => 'weekdays','amount' => 10, 'curruncy' => '£', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
  ['type' => 'weekends','amount' => 15, 'curruncy' => '£', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
];
